var formulario = {
			index: window.localStorage.getItem("formulario:index"),
			$table: document.getElementById("formulario-table"),
			$form: document.getElementById("formulario-form"),
			$button_save: document.getElementById("formulario-op-save"),
			$button_discard: document.getElementById("formulario-op-discard"),

			init: function() {
				// initialize storage index
				if (!formulario.index) {
					window.localStorage.setItem("formulario:index", formulario.index = 1);
				}

				// initialize form
				formulario.$form.reset();
				formulario.$button_discard.addEventListener("click", function(event) {
					formulario.$form.reset();
					formulario.$form.id_entry.value = 0;
				}, true);
				formulario.$form.addEventListener("submit", function(event) {
					var entry = {
						id: parseInt(this.id_entry.value),
						first_name: this.first_name.value,
						last_name: this.last_name.value,
						email: this.email.value
					};
					if (entry.id == 0) { // add
						formulario.storeAdd(entry);
						formulario.tableAdd(entry);
					}
					
					this.reset();
					this.id_entry.value = 0;
					event.preventDefault();
				}, true);

				// initialize table
				if (window.localStorage.length - 1) {
					var formulario_list = [], i, key;
					for (i = 0; i < window.localStorage.length; i++) {
						key = window.localStorage.key(i);
						if (/formulario:\d+/.test(key)) {
							formulario_list.push(JSON.parse(window.localStorage.getItem(key)));
						}
					}

					if (formulario_list.length) {
						formulario_list
							.sort(function(a, b) {
								return a.id < b.id ? -1 : (a.id > b.id ? 1 : 0);
							})
							.forEach(formulario.tableAdd);
					}
				}
				formulario.$table.addEventListener("click", function(event) {
					var op = event.target.getAttribute("data-op");
					if (/remove/.test(op)) {
						var entry = JSON.parse(window.localStorage.getItem("formulario:"+ event.target.getAttribute("data-id")));
						else if (op == "remove") {
							if (confirm('Esta seguro de borrar "'+ entry.first_name +' '+ entry.last_name +'" de la lista?')) {
								formulario.storeRemove(entry);
								formulario.tableRemove(entry);
							}
						}
						event.preventDefault();
					}
				}, true);
			},

			storeAdd: function(entry) {
				entry.id = formulario.index;
				window.localStorage.setItem("formulario:index", ++formulario.index);
				window.localStorage.setItem("formulario:"+ entry.id, JSON.stringify(entry));
			},
			storeEdit: function(entry) {
				window.localStorage.setItem("formulario:"+ entry.id, JSON.stringify(entry));
			},
			storeRemove: function(entry) {
				window.localStorage.removeItem("formulario:"+ entry.id);
			},

			tableAdd: function(entry) {
				var $tr = document.createElement("tr"), $td, key;
				for (key in entry) {
					if (entry.hasOwnProperty(key)) {
						$td = document.createElement("td");
						$td.appendChild(document.createTextNode(entry[key]));
						$tr.appendChild($td);
					}
				}
				$td = document.createElement("td");
				$td.innerHTML = '<a data-op="edit" data-id="'+ entry.id +'">Edit</a> | <a data-op="remove" data-id="'+ entry.id +'">Remove</a>';
				$tr.appendChild($td);
				$tr.setAttribute("id", "entry-"+ entry.id);
				formulario.$table.appendChild($tr);
			},
				$td = document.createElement("td");
				$td.innerHTML = '<a data-op="remove" data-id="'+ entry.id +'">Remove</a>';
				$tr.appendChild($td);
			},
			tableRemove: function(entry) {
				formulario.$table.removeChild(document.getElementById("entry-"+ entry.id));
			}
		};
		formulario.init();